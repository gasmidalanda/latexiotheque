\documentclass[french,12pt]{article}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{geometry}
\usepackage{amsmath}
\usepackage{tcolorbox}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{accents}
\usepackage{enumitem}
\usepackage{icomma}
\usepackage{setspace}
\usepackage{graphicx}
\usepackage{colortbl}
\usepackage{fancybox}

\usepackage{tikz}
\usepackage{OutilsGeomTikz}
\usepackage{tkz-euclide}

\definecolor{lightgray}{gray}{0.95}
\definecolor{darkgray}{gray}{0.75}
\definecolor{myblue}{RGB}{0,128,255}
\definecolor{mygreen}{RGB}{0, 128, 0}
\definecolor{myred}{RGB}{255, 0, 0}
\definecolor{bordeaux}{RGB}{128,0,32}
\usepackage{fourier}

\usepackage{sectsty}
\allsectionsfont{\sffamily}

\newenvironment{solution}
{\renewcommand\qedsymbol{$\blacksquare$}
	\begin{proof}[Solution]}
	{\end{proof}}
\renewcommand\qedsymbol{$\blacksquare$}

\newcommand{\ubar}[1]{\underaccent{\bar}{#1}}

\renewcommand\familydefault{\sfdefault}

\geometry{%
	a4paper,
	twoside, % Enable two-sided mode
	body={183.5mm,265mm},  % 195mm (original) - 11.5mm (increase) = 183.5mm
	inner=20mm,  % Margin for the side near the binding
	outer=8.5mm, % Margin for the side far from the binding
	top=15mm,
	headheight=9mm, headsep=3mm}

\pagestyle{fancy}
\fancyhf{}
\lhead{Collège Victor Vasarely \\ M. Iaconelli}
\chead{La symétrie centrale \\ Leçon}
\rhead{Mathématiques - 5e \\ 2023-2024}
\cfoot{\thepage\ sur \pageref{LastPage}}
\rfoot{\texttt{lmdbt.fr} | Sésamath}
\usepackage{ccicons}\lfoot{\ccby}

\setstretch{1}

\setlength\parindent{0pt}

\usepackage{xcolor}
\usepackage{titlesec}
\titleformat{\section}{\color{myred!70}\normalfont\Large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\color{myblue!70}\normalfont\large\bfseries}{\thesubsection}{1em}{}
\titleformat{\subsubsection}{\color{mygreen!70}\normalfont\normalsize\bfseries}{\thesubsubsection}{1em}{}

\newcommand{\drawCross}[2]{
	\draw[line width=1pt] (#1,#2) -- ++(-0.1,-0.1);
	\draw[line width=1pt] (#1,#2) -- ++(0.1,0.1);
	\draw[line width=1pt] (#1,#2) -- ++(-0.1,0.1);
	\draw[line width=1pt] (#1,#2) -- ++(0.1,-0.1);
}

\newcommand{\drawCrossP}[1]{
	\path (#1) coordinate (coord);
	\draw[line width=1pt] (coord) -- ++(-0.1,-0.1);
	\draw[line width=1pt] (coord) -- ++(0.1,0.1);
	\draw[line width=1pt] (coord) -- ++(-0.1,0.1);
	\draw[line width=1pt] (coord) -- ++(0.1,-0.1);
}

\newcommand{\grayovalbox}[1]{
	\begin{tikzpicture}[baseline=-0.4em]
		\node[draw, rounded corners=8pt, fill=lightgray, inner sep=4pt] {#1};
	\end{tikzpicture}
}

\newcommand{\redovalbox}[1]{
	\begin{tikzpicture}[baseline=-0.4em]
		\node[draw, rounded corners=8pt, fill=myred!20, inner sep=4pt] {#1};
	\end{tikzpicture}
}

\newcommand{\blueovalbox}[1]{
	\begin{tikzpicture}[baseline=-0.4em]
		\node[draw, rounded corners=8pt, fill=myblue!20, inner sep=4pt] {#1};
	\end{tikzpicture}
}

% Créer une boîte arrondie verte avec tcolorbox
\newtcolorbox{greenovalbox}[1][]{
	colback=mygreen!5,
	colframe=black,
	boxrule=0.5pt,
	arc=8pt,
	left=4pt,
	right=4pt,
	top=4pt,
	bottom=4pt,
	box align=base,
	#1
}


\newcommand{\vcentered}[1]{\begingroup
	\setbox0=\hbox{#1}\parbox{\dimexpr\ht0+\dp0\relax}{\box0}
	\endgroup}

\begin{document}
	\begin{minipage}{0.8\textwidth}
		\begin{center}
			\setlength{\fboxsep}{2mm} % définir l'écart
			\setlength{\fboxrule}{0.5mm} % définir l'épaisseur du trait
			\textcolor{bordeaux}{\fcolorbox{bordeaux}{white}{\LARGE{\textbf{La symétrie centrale - Leçon}}}}
		\end{center}
	\end{minipage}
	\hfill
	\begin{minipage}{0.15\textwidth}
		\begin{center}
			\includegraphics[width=0.9\textwidth]{SymetrieCentrale.png}
		\end{center}
	\end{minipage}
	\medskip
	
	\section{Construire le symétrique d'un point} \medskip
	
	\begin{center}
		\begin{minipage}{0.95\textwidth}
			\hspace{0.5cm}\ovalbox{\textbf{Propriété}}\vspace{-0.05cm} \\
			\ovalbox{\begin{minipage}{1\textwidth}
					\medskip \doublespacing 
					Deux points $A$ et $A'$ sont symétriques par rapport au point $O$ lorsque le point $O$ est le \makebox[5cm]\dotfill du segment $[AA']$.
					\medskip
			\end{minipage}}
		\end{minipage}
	\end{center}
	
	
	\begin{center}
		\begin{minipage}{0.95\textwidth}
			\hspace{0.5cm}\ovalbox{\textbf{Méthode}}\vspace{-0.3cm}
			\begin{greenovalbox}
			\begin{minipage}{1\textwidth}
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (A) at (0,0);
							\coordinate (O) at (3,1);
							
							% Dessiner la demi-droite [AO)
							\draw[-,line width=1pt] (A) -- ($(A)!2.25!(O)$);
							
							% Ajouter les points A et O
							\drawCross{0}{0}
							\drawCross{3}{1}
							\draw (A) node[above] {$A$};
							\draw (O) node[above] {$O$};
							
							\pgfmathsetmacro{\angle}{atan(1/3)}
							
							\coordinate (Crayon1) at ($(A)!2.25!(O)$);
							
							\tkzCrayon[Origine={Crayon1},Echelle=0.75,Longueur=2,Rotation=-30,Couleur=black]
							\tkzRegle[Largeur=1.25,Longueur=8,Couleur=gray,Rotation=\angle,Origine={(0,0)},AfficheValeurs=false,Fond,CouleurFond=myblue!75!white]
						\end{tikzpicture}
						
						On trace la demi-droite $[AO)$.\medskip
						
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (A) at (0,0);
							\coordinate (O) at (3,1);
							\coordinate (Ap) at ($(A)!2!(O)$);
							
							% Dessiner la demi-droite [AO)
							\draw[-,line width=1pt] (A) -- ($(A)!2.25!(O)$);
							
							% Ajouter les points A et O
							\drawCross{0}{0}
							\drawCross{3}{1}
							\draw (A) node[above] {$A$};
							\draw (O) node[above right] {$O$};
							
							\pgfmathsetmacro{\angle}{atan(1/3)}
							
							\draw[thick,red] 
							let \p1 = ($(O)-(Ap)$), \n2 = {veclen(\x1,\y1)}
							in ([shift=(\angle-10:\n2)]O) 
							arc (\angle-10:\angle+10:\n2);
							% Calculer les coordonnées du point sur l'arc à +10°
							
							% Récupérer les coordonnées de Ap
							\path (Ap) coordinate (ApCoords);
							\pgfgetlastxy{\Apx}{\Apy}
							
							% Calculer le rayon OA'
							\pgfmathsetmacro{\rOA}{veclen(\Apx-3,\Apy-1)}
							
							% Calculer les coordonnées du point sur l'arc
							\path (O) ++(+28:\rOA) coordinate (A3);
							
							\tkzCompas[LongueurBranches=4,LongueurCrayon=2,CouleurCrayon=red]{O}{A3}
						\end{tikzpicture} \\
						On trace un arc de cercle de centre $O$ et de rayon $OA$. Il coupe la demi-droite $[AO)$ en un point.
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.3\textwidth}
						\texttt{\\} \texttt{\\} \texttt{\\}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (A) at (0,0);
							\coordinate (O) at (3,1);
							\coordinate (Ap) at ($(A)!2!(O)$);
							
							% Dessiner la demi-droite [AO)
							\draw[-,line width=1pt] (A) -- ($(A)!2.25!(O)$);
							
							% Ajouter les points A et O
							\drawCross{0}{0}
							\drawCross{3}{1}
							\draw (A) node[above] {$A$};
							\draw (O) node[above right] {$O$};
							
							\pgfmathsetmacro{\angle}{atan(1/3)}
							
							\draw[thick,red] 
							let \p1 = ($(O)-(Ap)$), \n2 = {veclen(\x1,\y1)}
							in ([shift=(\angle-10:\n2)]O) 
							arc (\angle-10:\angle+10:\n2);
							% Calculer les coordonnées du point sur l'arc à +10°
							
							% Récupérer les coordonnées de Ap
							\path (Ap) coordinate (ApCoords);
							\pgfgetlastxy{\Apx}{\Apy}
							
							\drawCross{6}{2}
							\draw (Ap) node[below right] {$A'$};
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(A)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(A)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.48!(Ap)$)!0.15cm!90:(Ap)$) -- ($($(O)!0.48!(Ap)$)!0.15cm!-90:(Ap)$);
							\draw[line width=0.75pt] ($($(A)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(A)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.52!(Ap)$)!0.15cm!90:(Ap)$) -- ($($(O)!0.52!(Ap)$)!0.15cm!-90:(Ap)$);
						\end{tikzpicture}
						\texttt{\\} \texttt{\\}
						On place le point $A'$ à l'intersection de la demi-droite $[AO)$ et de l'arc de cercle. On code la figure.
					\end{minipage}
			\end{minipage}
			\end{greenovalbox}
		\end{minipage}
	\end{center}
	
	\section{Construire le symétrique d'un segment} \medskip
	
	
	\begin{center}
		\begin{minipage}{0.95\textwidth}
			\hspace{0.5cm}\ovalbox{\textbf{Méthode}}\vspace{-0.3cm}
			\begin{greenovalbox}\begin{minipage}{1\textwidth}
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (O) at (0,0);
							\coordinate (C) at (-1,+2);
							\coordinate (D) at (-3,-1);
							\coordinate (Cp) at (+1,-2);
							
							% Extraire les coordonnées de A
							\path (Cp); \pgfgetlastxy{\Cpx}{\Cpy};
							
							% Calcul des coordonnées de P après rotation de 20° autour de O en maintenant la même distance
							\coordinate (P) at ({cos(10)*1 - sin(10)*(-2)},{sin(10)*1 + cos(10)*(-2)});
							
							% Tracer les arcs autour de C'
							\draw[red,line width=1pt] (Cp) arc[start angle={atan2(\Cpy,\Cpx)}, end angle={atan2(sin(25)*\Cpx + cos(25)*\Cpy,cos(25)*\Cpx - sin(25)*\Cpy)}, radius={sqrt(\Cpx*\Cpx + \Cpy*\Cpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Cp) arc[start angle={atan2(\Cpy,\Cpx)}, end angle={atan2(sin(-20)*\Cpx + cos(-20)*\Cpy,cos(-20)*\Cpx - sin(-20)*\Cpy)}, radius={sqrt(\Cpx*\Cpx + \Cpy*\Cpy)}];
							
							\drawCross{0}{0}
							\drawCross{-1}{+2}
							\drawCross{-3}{-1}
							\drawCross{+1}{-2}
							
							\draw (O) node[below left] {$O$};
							\draw (C) node[above] {$C$};
							\draw (D) node[above] {$D$};
							\draw (Cp) node[right] {$C'$};
							
							\draw[-,line width=1pt] (D) -- (C);
							\draw[dashed,line width=1pt] (C) -- ($(C)!2.5!(O)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(C)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.48!(Cp)$)!0.15cm!90:(Cp)$) -- ($($(O)!0.48!(Cp)$)!0.15cm!-90:(Cp)$);
							\draw[line width=0.75pt] ($($(C)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.52!(Cp)$)!0.15cm!90:(Cp)$) -- ($($(O)!0.52!(Cp)$)!0.15cm!-90:(Cp)$);
							
							\tkzCompas[LongueurBranches=4,LongueurCrayon=2,CouleurCrayon=red]{O}{P}
						\end{tikzpicture}
						
						On construit le point $C'$ symétrique du point $C$ par rapport au point $O$.\medskip
						
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (O) at (0,0);
							\coordinate (C) at (-1,+2);
							\coordinate (D) at (-3,-1);
							\coordinate (Dp) at (+3,+1);
							\coordinate (Cp) at (+1,-2);
							
							% Extraire les coordonnées de Cp
							\path (Cp); \pgfgetlastxy{\Cpx}{\Cpy};
							
							% Extraire les coordonnées de Dp
							\path (Dp); \pgfgetlastxy{\Dpx}{\Dpy};
							
							% Calcul des coordonnées de P après rotation de 20° autour de O en maintenant la même distance
							\coordinate (P) at ({cos(10)*3 - sin(10)*(1)},{sin(10)*3 + cos(10)*(1)});
							
							% Tracer les arcs autour de C'
							\draw[red,line width=1pt] (Cp) arc[start angle={atan2(\Cpy,\Cpx)}, end angle={atan2(sin(25)*\Cpx + cos(25)*\Cpy,cos(25)*\Cpx - sin(25)*\Cpy)}, radius={sqrt(\Cpx*\Cpx + \Cpy*\Cpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Cp) arc[start angle={atan2(\Cpy,\Cpx)}, end angle={atan2(sin(-25)*\Cpx + cos(-25)*\Cpy,cos(-25)*\Cpx - sin(-25)*\Cpy)}, radius={sqrt(\Cpx*\Cpx + \Cpy*\Cpy)}];
							
							% Tracer les arcs autour de D'
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(25)*\Dpx + cos(25)*\Dpy,cos(25)*\Dpx - sin(25)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(-25)*\Dpx + cos(-25)*\Dpy,cos(-25)*\Dpx - sin(-25)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							
							\drawCross{0}{0}
							\drawCross{-1}{+2}
							\drawCross{-3}{-1}
							\drawCross{+1}{-2}
							\drawCross{+3}{+1}
							
							\draw (O) node[below left] {$O$};
							\draw (C) node[above] {$C$};
							\draw (D) node[above] {$D$};
							\draw (Cp) node[right] {$C'$};
							\draw (Dp) node[below right] {$D'$};
							
							\draw[line width=1pt] (D) -- (C);
							\draw[dashed,line width=1pt] (C) -- ($(C)!2.5!(O)$);
							\draw[dashed,line width=1pt] (D) -- ($(D)!2.5!(O)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(C)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.48!(Cp)$)!0.15cm!90:(Cp)$) -- ($($(O)!0.48!(Cp)$)!0.15cm!-90:(Cp)$);
							\draw[line width=0.75pt] ($($(C)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.52!(Cp)$)!0.15cm!90:(Cp)$) -- ($($(O)!0.52!(Cp)$)!0.15cm!-90:(Cp)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(D)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(D)!0.5!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Dp)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(Dp)!0.5!(O)$)!0.15cm!-90:(O)$);
							
							\tkzCompas[LongueurBranches=4,LongueurCrayon=2,CouleurCrayon=red]{O}{P}
						\end{tikzpicture}
						
						On construit le point $D'$ symétrique du point $D$ par rapport au point $O$.\medskip
						
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (O) at (0,0);
							\coordinate (C) at (-1,+2);
							\coordinate (D) at (-3,-1);
							\coordinate (Dp) at (+3,+1);
							\coordinate (Cp) at (+1,-2);
							
							% Extraire les coordonnées de Cp
							\path (Cp); \pgfgetlastxy{\Cpx}{\Cpy};
							
							% Extraire les coordonnées de Dp
							\path (Dp); \pgfgetlastxy{\Dpx}{\Dpy};
							
							% Calcul des coordonnées de P après rotation de 20° autour de O en maintenant la même distance
							\coordinate (P) at ({cos(10)*3 - sin(10)*(1)},{sin(10)*3 + cos(10)*(1)});
							
							% Tracer les arcs autour de C'
							\draw[red,line width=1pt] (Cp) arc[start angle={atan2(\Cpy,\Cpx)}, end angle={atan2(sin(25)*\Cpx + cos(25)*\Cpy,cos(25)*\Cpx - sin(25)*\Cpy)}, radius={sqrt(\Cpx*\Cpx + \Cpy*\Cpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Cp) arc[start angle={atan2(\Cpy,\Cpx)}, end angle={atan2(sin(-25)*\Cpx + cos(-25)*\Cpy,cos(-25)*\Cpx - sin(-25)*\Cpy)}, radius={sqrt(\Cpx*\Cpx + \Cpy*\Cpy)}];
							
							% Tracer les arcs autour de D'
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(25)*\Dpx + cos(25)*\Dpy,cos(25)*\Dpx - sin(25)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(-25)*\Dpx + cos(-25)*\Dpy,cos(-25)*\Dpx - sin(-25)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							
							\drawCross{0}{0}
							\drawCross{-1}{+2}
							\drawCross{-3}{-1}
							\drawCross{+1}{-2}
							\drawCross{+3}{+1}
							
							\draw (O) node[below left] {$O$};
							\draw (C) node[above] {$C$};
							\draw (D) node[above] {$D$};
							\draw (Cp) node[right] {$C'$};
							\draw (Dp) node[below right] {$D'$};
							
							\draw[line width=1pt] (D) -- (C);
							\draw[dashed,line width=1pt] (C) -- ($(C)!2.5!(O)$);
							\draw[dashed,line width=1pt] (D) -- ($(D)!2.5!(O)$);
							\draw[line width=1pt,myblue] (Dp) -- (Cp);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(C)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.48!(Cp)$)!0.15cm!90:(Cp)$) -- ($($(O)!0.48!(Cp)$)!0.15cm!-90:(Cp)$);
							\draw[line width=0.75pt] ($($(C)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.52!(Cp)$)!0.15cm!90:(Cp)$) -- ($($(O)!0.52!(Cp)$)!0.15cm!-90:(Cp)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(D)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(D)!0.5!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Dp)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(Dp)!0.5!(O)$)!0.15cm!-90:(O)$);
						\end{tikzpicture}
						
						On trace le segment $[C'D']$.\medskip
						
					\end{minipage}
			\end{minipage}\end{greenovalbox}
		\end{minipage}
	\end{center}
	
	\section{Construire le symétrique d'un cercle} \medskip
	
	\begin{center}
		\begin{minipage}{0.95\textwidth}
			\hspace{0.5cm}\ovalbox{\textbf{Méthode}}\vspace{-0.3cm}
			\begin{greenovalbox}\begin{minipage}{1\textwidth}
					\begin{minipage}{0.48\textwidth}
						\hfill
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (M) at (0,0);
							\coordinate (O) at (-3,+1);
							\coordinate (Op) at (3,-1);
							
							% Extraire les coordonnées de Op
							\path (Op); \pgfgetlastxy{\Opx}{\Opy};
							
							% Tracer un cercle de centre O et de rayon 1.5
							\draw[line width=1pt] (O) circle (2);
							
							% Calculer et définir le point A
							\coordinate (A) at ({-3 + 2*cos(-45)}, {1 - 2*sin(-45)});
							
							% Calcul des coordonnées de P après rotation de 20° autour de M
							\coordinate (P) at ({cos(10)*3 - sin(10)*(-1)},{sin(10)*3 + cos(10)*(-1)});
							
							% Tracer les arcs autour de C'
							\draw[red,line width=1pt] (Op) arc[start angle={atan2(\Opy,\Opx)}, end angle={atan2(sin(25)*\Opx + cos(25)*\Opy,cos(25)*\Opx - sin(25)*\Opy)}, radius={sqrt(\Opx*\Opx + \Opy*\Opy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Op) arc[start angle={atan2(\Opy,\Opx)}, end angle={atan2(sin(-25)*\Opx + cos(-25)*\Opy,cos(-25)*\Opx - sin(-25)*\Opy)}, radius={sqrt(\Opx*\Opx + \Opy*\Opy)}];
							
							\drawCross{0}{0}
							\drawCross{-3}{1}
							\drawCross{+3}{-1}
							
							\draw (O) node[below left] {$O$};
							\draw (M) node[below] {$M$};
							\draw (Op) node[below left] {$O'$};
							\draw (-5,+3) node {$\mathcal{C}$};
							
							\draw[dashed,line width=1pt] (O) -- ($(O)!2.5!(M)$);
							\draw[line width=1pt] (O) -- (A);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(M)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(M)!0.5!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt];
							\draw[line width=0.75pt] ($($(M)!0.5!(Op)$)!0.15cm!90:(Op)$) -- ($($(M)!0.5!(Op)$)!0.15cm!-90:(Op)$);
							
							\tkzCompas[LongueurBranches=4,LongueurCrayon=2,CouleurCrayon=red]{M}{P}
						\end{tikzpicture}
						\hfill$~$
						
						On construit le point $O'$ symétrique du point $O$ par rapport au point $M$.\medskip
						
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.48\textwidth}
						\hfill
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (M) at (0,0);
							\coordinate (O) at (-3,+1);
							\coordinate (Op) at (3,-1);
							
							% Extraire les coordonnées de Op
							\path (Op); \pgfgetlastxy{\Opx}{\Opy};
							
							% Tracer un cercle de centre O et de rayon 1.5
							\draw[line width=1pt] (O) circle (2);
							\draw[line width=1pt,myblue] (Op) circle (2);
							
							% Calculer et définir le point A
							\coordinate (A) at ({-3 + 2*cos(-45)}, {1 - 2*sin(-45)});
							
							\coordinate (Ap) at ({3 + 2*cos(-45)}, {-1 - 2*sin(-45)});
							
							% Calcul des coordonnées de P après rotation de 20° autour de M
							\coordinate (P) at ({cos(10)*3 - sin(10)*(-1)},{sin(10)*3 + cos(10)*(-1)});
							
							% Tracer les arcs autour de C'
							\draw[red,line width=1pt] (Op) arc[start angle={atan2(\Opy,\Opx)}, end angle={atan2(sin(25)*\Opx + cos(25)*\Opy,cos(25)*\Opx - sin(25)*\Opy)}, radius={sqrt(\Opx*\Opx + \Opy*\Opy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Op) arc[start angle={atan2(\Opy,\Opx)}, end angle={atan2(sin(-25)*\Opx + cos(-25)*\Opy,cos(-25)*\Opx - sin(-25)*\Opy)}, radius={sqrt(\Opx*\Opx + \Opy*\Opy)}];
							
							\drawCross{0}{0}
							\drawCross{-3}{1}
							\drawCross{+3}{-1}
							
							\draw (O) node[below left] {$O$};
							\draw (M) node[below] {$M$};
							\draw (Op) node[below left] {$O'$};
							\draw (-5,+3) node {$\mathcal{C}$};
							\draw (+5,-3) node {$\mathcal{C}'$};
							
							\draw[dashed,line width=1pt] (O) -- ($(O)!2.5!(M)$);
							\draw[line width=1pt] (O) -- (A);
							\draw[line width=1pt] (Op) -- (Ap);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(M)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(M)!0.5!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt];
							\draw[line width=0.75pt] ($($(M)!0.5!(Op)$)!0.15cm!90:(Op)$) -- ($($(M)!0.5!(Op)$)!0.15cm!-90:(Op)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(A)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(A)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt];
							\draw[line width=0.75pt] ($($(A)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(A)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Ap)!0.48!(Op)$)!0.15cm!90:(Op)$) -- ($($(Ap)!0.48!(Op)$)!0.15cm!-90:(Op)$);
							\draw[line width=0.75pt];
							\draw[line width=0.75pt] ($($(Ap)!0.52!(Op)$)!0.15cm!90:(Op)$) -- ($($(Ap)!0.52!(Op)$)!0.15cm!-90:(Op)$);
							
						\end{tikzpicture}
						\hfill$~$
						
						$\mathcal{C}'$ est le cercle de centre $O'$ et de même rayon que le cercle $\mathcal{C}$.
						
					\end{minipage}
			\end{minipage}\end{greenovalbox}
		\end{minipage}
	\end{center}
	
	\section{Construire le symétrique d'une figure} \medskip
	
	\begin{center}
		\begin{minipage}{0.95\textwidth}
			\hspace{0.5cm}\ovalbox{\textbf{Propriété}}\vspace{-0.05cm} \\
			\ovalbox{\begin{minipage}{1\textwidth}
					\medskip \doublespacing 
					Le symétrique d'une figure par rapport à un point s'obtient par un \makebox[5cm]\dotfill autour de ce point. On obtient donc une figure qui lui est \makebox[5cm]\dotfill.
					\medskip
			\end{minipage}}
		\end{minipage}
	\end{center}
	
	\begin{center}
		\begin{minipage}{0.95\textwidth}
			\hspace{0.5cm}\ovalbox{\textbf{Méthode}}\vspace{-0.3cm}
			\begin{greenovalbox}\begin{minipage}{1\textwidth}
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (O) at (0,0);
							\coordinate (A) at (-1,+2);
							\coordinate (B) at (-3,+3);
							\coordinate (C) at (-3,0);
							\coordinate (D) at (-3,-1.5);
							\coordinate (Ap) at (+1,-2);
							\coordinate (Bp) at (+3,-3);
							
							% Extraire les coordonnées de A
							\path (Ap); \pgfgetlastxy{\Apx}{\Apy};
							% Extraire les coordonnées de A
							\path (Bp); \pgfgetlastxy{\Bpx}{\Bpy};
							
							
							% Calcul des coordonnées de P après rotation de 20° autour de O en maintenant la même distance
							\coordinate (P) at ({cos(10)*(3) - sin(10)*(-3)},{sin(10)*(+3) + cos(10)*(-3)});
							
							% Tracer les arcs autour de A' et B'
							\draw[red,line width=1pt] (Ap) arc[start angle={atan2(\Apy,\Apx)}, end angle={atan2(sin(25)*\Apx + cos(25)*\Apy,cos(25)*\Apx - sin(25)*\Apy)}, radius={sqrt(\Apx*\Apx + \Apy*\Apy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Ap) arc[start angle={atan2(\Apy,\Apx)}, end angle={atan2(sin(-20)*\Apx + cos(-20)*\Apy,cos(-20)*\Apx - sin(-20)*\Apy)}, radius={sqrt(\Apx*\Apx + \Apy*\Apy)}];
							
							\draw[red,line width=1pt] (Bp) arc[start angle={atan2(\Bpy,\Bpx)}, end angle={atan2(sin(25)*\Bpx + cos(25)*\Bpy,cos(25)*\Bpx - sin(25)*\Bpy)}, radius={sqrt(\Bpx*\Bpx + \Bpy*\Bpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Bp) arc[start angle={atan2(\Bpy,\Bpx)}, end angle={atan2(sin(-20)*\Bpx + cos(-20)*\Bpy,cos(-20)*\Bpx - sin(-20)*\Bpy)}, radius={sqrt(\Bpx*\Bpx + \Bpy*\Bpy)}];
							
							
							\drawCross{0}{0}
							\drawCross{-1}{+2}
							\drawCross{-3}{+3}
							\drawCross{-3}{0}
							\drawCross{-3}{-1.5}
							\drawCross{+1}{-2}
							\drawCross{+3}{-3}
							
							\draw (O) node[below left] {$O$};
							\draw (A) node[above] {$A$};
							\draw (B) node[left] {$B$};
							\draw (C) node[left] {$C$};
							\draw (D) node[left] {$D$};
							\draw (Ap) node[left] {$A'$};
							\draw (Bp) node[right] {$B'$};
							
							\draw[line width=1pt] (A) -- (B);
							\draw[line width=1pt] (D) -- (B);
							\draw[line width=1pt] (C) -- (A);
							\draw[dashed,line width=1pt] (A) -- ($(A)!2.25!(O)$);
							\draw[dashed,line width=1pt] (B) -- ($(B)!2.25!(O)$);
							\draw[line width=1pt,myblue] (Ap) -- (Bp);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(A)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(A)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.48!(Ap)$)!0.15cm!90:(Ap)$) -- ($($(O)!0.48!(Ap)$)!0.15cm!-90:(Ap)$);
							\draw[line width=0.75pt] ($($(A)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(A)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(O)!0.52!(Ap)$)!0.15cm!90:(Ap)$) -- ($($(O)!0.52!(Ap)$)!0.15cm!-90:(Ap)$);
							
							\draw[line width=0.75pt] ($($(B)!0.6!(O)$)!0.15cm!90:(O)$) -- ($($(B)!0.6!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Bp)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(Bp)!0.5!(O)$)!0.15cm!-90:(O)$);
							
							\tkzCompas[LongueurBranches=4,LongueurCrayon=2,CouleurCrayon=red]{O}{P}
						\end{tikzpicture}
						
						On construit les points $A'$ et $B'$, symétriques des points $A$ et $B$ par rapport a $O$. On trace le segment $[A'B']$.\medskip
						
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.3\textwidth}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (O) at (0,0);
							\coordinate (A) at (-1,+2);
							\coordinate (B) at (-3,+3);
							\coordinate (C) at (-3,0);
							\coordinate (D) at (-3,-1.5);
							\coordinate (Ap) at (+1,-2);
							\coordinate (Bp) at (+3,-3);
							\coordinate (Dp) at (+3,+1.5);
							
							% Extraire les coordonnées de A et B
							\path (Ap); \pgfgetlastxy{\Apx}{\Apy};
							\path (Bp); \pgfgetlastxy{\Bpx}{\Bpy};
							
							% Extraire les coordonnées de D
							\path (Dp); \pgfgetlastxy{\Dpx}{\Dpy};
							
							
							% Calcul des coordonnées de P après rotation de 20° autour de O en maintenant la même distance
							\coordinate (P) at ({cos(10)*(3) - sin(10)*(1.5)},{sin(10)*(+3) + cos(10)*(1.5)});
							
							% Tracer les arcs autour de A' et B'
							\draw[red,line width=1pt] (Ap) arc[start angle={atan2(\Apy,\Apx)}, end angle={atan2(sin(25)*\Apx + cos(25)*\Apy,cos(25)*\Apx - sin(25)*\Apy)}, radius={sqrt(\Apx*\Apx + \Apy*\Apy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Ap) arc[start angle={atan2(\Apy,\Apx)}, end angle={atan2(sin(-20)*\Apx + cos(-20)*\Apy,cos(-20)*\Apx - sin(-20)*\Apy)}, radius={sqrt(\Apx*\Apx + \Apy*\Apy)}];
							
							\draw[red,line width=1pt] (Bp) arc[start angle={atan2(\Bpy,\Bpx)}, end angle={atan2(sin(25)*\Bpx + cos(25)*\Bpy,cos(25)*\Bpx - sin(25)*\Bpy)}, radius={sqrt(\Bpx*\Bpx + \Bpy*\Bpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Bp) arc[start angle={atan2(\Bpy,\Bpx)}, end angle={atan2(sin(-20)*\Bpx + cos(-20)*\Bpy,cos(-20)*\Bpx - sin(-20)*\Bpy)}, radius={sqrt(\Bpx*\Bpx + \Bpy*\Bpy)}];
							
							% Tracer les arcs autour de D'
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(25)*\Dpx + cos(25)*\Dpy,cos(25)*\Dpx - sin(25)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(-20)*\Dpx + cos(-20)*\Dpy,cos(-20)*\Dpx - sin(-20)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							
							
							
							\drawCross{0}{0}
							\drawCross{-1}{+2}
							\drawCross{-3}{+3}
							\drawCross{-3}{0}
							\drawCross{-3}{-1.5}
							\drawCross{+1}{-2}
							\drawCross{+3}{-3}
							\drawCross{+3}{+1.5}
							
							\draw (O) node[below left] {$O$};
							\draw (A) node[above] {$A$};
							\draw (B) node[left] {$B$};
							\draw (C) node[left] {$C$};
							\draw (D) node[left] {$D$};
							\draw (Ap) node[left] {$A'$};
							\draw (Bp) node[right] {$B'$};
							\draw (Dp) node[right] {$D'$};
							
							\draw[line width=1pt] (A) -- (B);
							\draw[line width=1pt] (D) -- (B);
							\draw[line width=1pt] (C) -- (A);
							\draw[line width=1pt,myblue] (Ap) -- (Bp);
							\draw[line width=1pt,myblue] (Dp) -- (Bp);
							\draw[dashed,line width=1pt] (D) -- ($(D)!2.25!(O)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(D)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(D)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Dp)!0.48!(O)$)!0.15cm!90:(O)$) -- ($($(Dp)!0.52!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(D)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(D)!0.48!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Dp)!0.52!(O)$)!0.15cm!90:(O)$) -- ($($(Dp)!0.48!(O)$)!0.15cm!-90:(O)$);
							
							\tkzCompas[LongueurBranches=4,LongueurCrayon=2,CouleurCrayon=red]{O}{P}
						\end{tikzpicture}
						
						On construit le point $D'$,
						symétrique du point $D$ par
						rapport a $O$. On trace le
						segment $[B'D']$.\medskip
						
					\end{minipage}
					\hfill\vline\hfill
					\begin{minipage}{0.3\textwidth}
						\texttt{\\} \texttt{\\}
						\begin{tikzpicture}[scale=0.6]
							% Définir les points A et O
							\coordinate (O) at (0,0);
							\coordinate (A) at (-1,+2);
							\coordinate (B) at (-3,+3);
							\coordinate (C) at (-3,0);
							\coordinate (D) at (-3,-1.5);
							\coordinate (Ap) at (+1,-2);
							\coordinate (Bp) at (+3,-3);
							\coordinate (Dp) at (+3,+1.5);
							\coordinate (Cp) at (+3,0);
							
							% Extraire les coordonnées de A et B
							\path (Ap); \pgfgetlastxy{\Apx}{\Apy};
							\path (Bp); \pgfgetlastxy{\Bpx}{\Bpy};
							
							% Extraire les coordonnées de D
							\path (Dp); \pgfgetlastxy{\Dpx}{\Dpy};
							
							
							% Calcul des coordonnées de P après rotation de 20° autour de O en maintenant la même distance
							\coordinate (P) at ({cos(10)*(3) - sin(10)*(1.5)},{sin(10)*(+3) + cos(10)*(1.5)});
							
							% Tracer les arcs autour de A' et B'
							\draw[red,line width=1pt] (Ap) arc[start angle={atan2(\Apy,\Apx)}, end angle={atan2(sin(25)*\Apx + cos(25)*\Apy,cos(25)*\Apx - sin(25)*\Apy)}, radius={sqrt(\Apx*\Apx + \Apy*\Apy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Ap) arc[start angle={atan2(\Apy,\Apx)}, end angle={atan2(sin(-20)*\Apx + cos(-20)*\Apy,cos(-20)*\Apx - sin(-20)*\Apy)}, radius={sqrt(\Apx*\Apx + \Apy*\Apy)}];
							
							\draw[red,line width=1pt] (Bp) arc[start angle={atan2(\Bpy,\Bpx)}, end angle={atan2(sin(25)*\Bpx + cos(25)*\Bpy,cos(25)*\Bpx - sin(25)*\Bpy)}, radius={sqrt(\Bpx*\Bpx + \Bpy*\Bpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Bp) arc[start angle={atan2(\Bpy,\Bpx)}, end angle={atan2(sin(-20)*\Bpx + cos(-20)*\Bpy,cos(-20)*\Bpx - sin(-20)*\Bpy)}, radius={sqrt(\Bpx*\Bpx + \Bpy*\Bpy)}];
							
							% Tracer les arcs autour de D'
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(25)*\Dpx + cos(25)*\Dpy,cos(25)*\Dpx - sin(25)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							% Tracer l'arc AB de centre O
							\draw[red,line width=1pt] (Dp) arc[start angle={atan2(\Dpy,\Dpx)}, end angle={atan2(sin(-20)*\Dpx + cos(-20)*\Dpy,cos(-20)*\Dpx - sin(-20)*\Dpy)}, radius={sqrt(\Dpx*\Dpx + \Dpy*\Dpy)}];
							
							
							
							\drawCross{0}{0}
							\drawCross{-1}{+2}
							\drawCross{-3}{+3}
							\drawCross{-3}{0}
							\drawCross{-3}{-1.5}
							\drawCross{+1}{-2}
							\drawCross{+3}{-3}
							\drawCross{+3}{+1.5}
							\drawCross{+3}{0}
							
							\draw (O) node[below left] {$O$};
							\draw (A) node[above] {$A$};
							\draw (B) node[left] {$B$};
							\draw (C) node[left] {$C$};
							\draw (D) node[left] {$D$};
							\draw (Ap) node[left] {$A'$};
							\draw (Bp) node[right] {$B'$};
							\draw (Dp) node[above right] {$D'$};
							\draw (Cp) node[right] {$C'$};
							
							\draw[line width=1pt] (A) -- (B);
							\draw[line width=1pt] (D) -- (B);
							\draw[line width=1pt] (C) -- (A);
							\draw[line width=1pt,myblue] (Ap) -- (Bp);
							\draw[line width=1pt,myblue] (Dp) -- (Bp);
							\draw[line width=1pt,myblue] (Cp) -- (Ap);
							\draw[dashed,line width=1pt] (C) -- ($(C)!2!(O)$);
							
							% Codage égalité de longueur
							\draw[line width=0.75pt] ($($(C)!0.47!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.47!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(C)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.5!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(C)!0.53!(O)$)!0.15cm!90:(O)$) -- ($($(C)!0.53!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Cp)!0.47!(O)$)!0.15cm!90:(O)$) -- ($($(Cp)!0.47!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Cp)!0.5!(O)$)!0.15cm!90:(O)$) -- ($($(Cp)!0.5!(O)$)!0.15cm!-90:(O)$);
							\draw[line width=0.75pt] ($($(Cp)!0.53!(O)$)!0.15cm!90:(O)$) -- ($($(Cp)!0.53!(O)$)!0.15cm!-90:(O)$);
							
							\tkzRegle[Largeur=1.25,Longueur=7,Couleur=gray,Rotation=0,Origine={(-3,-0.025)},AfficheValeurs=false,Fond,CouleurFond=myblue!75!white]
							\tkzCrayon[Origine={Cp},Echelle=0.75,Longueur=2,Rotation=-30,Couleur=black]
						\end{tikzpicture}
						
						On construit le point $C'$,
						symétrique du point $C$ par
						rapport a $O$. On trace le
						segment $[A'C']$. \medskip
						
					\end{minipage}
			\end{minipage}\end{greenovalbox}
		\end{minipage}
	\end{center}
	
\end{document}

